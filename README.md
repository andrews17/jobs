# REPOSITORIO PARA PUBLICAÇÃO DE TESTES E EXERCÍCIOS

## Por: Andre Ricardo Cintra
## Email: arcin_es@hotmail.com

1. netflix-api
	1. npm install
	1. gulp compile (para compilar a pasta assets, se necessário)
	1. abrir a página diretamente no navegador

1. sample-api
	1. php artisan serve
	1. localhost:8000/test/{id} (POST, PUT e DELETE *precisam* informar um ID e os dados no FORM)