<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $json = json_decode(file_get_contents(__DIR__ . "../../../data.json"));
        return response()->json($json);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $id = $request->input("id");
        $nome = $request->input("nome");
        $email = $request->input("email");
        $data = array("id" => $id, "nome" => $nome, "email" => $email);

        $json = json_decode(file_get_contents(__DIR__ . "../../../data.json"));
        $json->items[] = $data;

        $fp = fopen(__DIR__ . "../../../data.json", "w+");
        fwrite($fp, json_encode($json));
        fclose($fp);

        return response()->json(array("mensagem" => "Cadastrado com sucesso!"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $json = json_decode(file_get_contents(__DIR__ . "../../../data.json"));
        foreach($json->items as $item){
            if($item->id == $id){
                return response()->json($item);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $json = json_decode(file_get_contents(__DIR__ . "../../../data.json"));
        foreach($json->items as $item){
            if($item->id == $id){
                $item->nome = $request->input("nome");
                $item->email = $request->input("email");
            }
        }

        $fp = fopen(__DIR__ . "../../../data.json", "w+");
        fwrite($fp, json_encode($json));
        fclose($fp);

        return response()->json(array("mensagem" => "Editado com sucesso!"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $json = json_decode(file_get_contents(__DIR__ . "../../../data.json"));
        foreach($json->items as $key => $item){
            if($item->id == $id){
                unset($json->items[$key]);
            }
        }

        $fp = fopen(__DIR__ . "../../../data.json", "w+");
        fwrite($fp, json_encode($json));
        fclose($fp);

        return response()->json(array("mensagem" => "Excluído com sucesso!"));
    }
}
