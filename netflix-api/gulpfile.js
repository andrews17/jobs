var gulp = require("gulp"),
    sass = require('gulp-sass'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    concat = require("gulp-concat")

gulp.task('css', function() {
    return gulp.src(['assets/src/css/**/*.css', 'assets/src/sass/style.scss'])
    .pipe(concat("main.css"))
    .pipe(sass())
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano())
    .pipe(gulp.dest('public/css'))
});

gulp.task('js', function() {
    return gulp.src(['assets/src/js/lib/*.js','assets/src/js/*.js'])
    .pipe(concat('main.js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('public/js'))
});

gulp.task('compile', function(){
    return gulp.start('css')
    .start('js')
});