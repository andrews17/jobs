function getNF(options){
    $(".title-list").html("Carregando...");
    urlAPI = "http://netflixroulette.net/api/api.php?";    
    dataAPI = "";

    $.each(options.filters, function(i, v){
        if(v.value !== undefined && v.value != "")
            dataAPI = dataAPI + v.field + "=" + v.value + "&";
    });

    $.ajax({
        url: urlAPI,
        type: "GET",
        data: dataAPI.replace(/\&$/,''),
        success: function(resp){
            $(".title-list").html("");
            $.each(resp, function(i, v){
                curItem = $("<div>", {class:"col s12 m6 l4 xl4 item"});
                leftSide = $("<div>", {class:"col s6 m6 l6 xl6 left-side"});
                rightSide = $("<div>", {class:"col s6 m6 l6 xl6 right-side"});
                thumb = $("<img>", {"src":v.poster});                
                $(leftSide).append(thumb);
                $(rightSide).append("<p> Título: " + v.show_title + "</p>");
                $(rightSide).append("<p> Avaliação: " + v.rating + "</p>");
                $(leftSide).append("<br /><button class='btn watch' data-link='https://www.netflix.com/watch/" + v.show_id + "'>Assitir</button>");
                $(curItem).append(leftSide);
                $(curItem).append(rightSide);
                $(".title-list").append(curItem);
                $(".watch").on("click", function(){
                    url = $(this)[0].dataset.link;
                    window.open(url);
                });
            });
        },
        error: function(resp){
            console.log(resp);
        }
    })
}

$(document).ready(function(){    

    $("#input-lowrating").on("change", function(e){        
        $("#label-lowrating").html("Nota mínima: " + $(this).val());
    });
    $("#input-highrating").on("change", function(e){        
        $("#label-highrating").html("Nota máxima: " + $(this).val());
    });

    $("#send-btn").on("click", function(e){
        
        options = { "filters" : [
                {"field" : "actor", "value" : $("#input-actor").val()},
                {"field" : "tv", "value" : $("#input-category").val()},
                {"field" : "movies", "value" : $("#input-category").val()},
                {"field" : "lowrating", "value" : $("#input-lowrating").val()},
                {"field" : "highrating", "value" : $("#input-highrating").val()},
                {"field" : "director", "value" : $("#input-director").val()},
                {"field" : "genre", "value" : $("#input-genre").val()}
            ]
        }
        getNF(options);

        e.preventDefault();
    })
});